# morse-code-translator

A CLI morse code translator.

##### Status: In development

When finished this morse code translator should be able to decode and encode morse code.

Currently it only encodes characters into morse code.
