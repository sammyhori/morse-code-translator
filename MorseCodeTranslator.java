import java.util.Scanner;
import java.util.HashMap;
import java.util.Map;

class Translator {
	final static Map<Character, Boolean[]> MORSE_LETTERS = Map.ofEntries(
			Map.entry('a', new Boolean[]{false, true}),
			Map.entry('b', new Boolean[]{true, false, false, false}),
			Map.entry('c', new Boolean[]{true, false, true, false}),
			Map.entry('d', new Boolean[]{true, false, false}),
			Map.entry('e', new Boolean[]{false}),
			Map.entry('f', new Boolean[]{false, false, true, false}),
			Map.entry('g', new Boolean[]{true, true, false}),
			Map.entry('h', new Boolean[]{false, false, false, false}),
			Map.entry('i', new Boolean[]{false, false}),
			Map.entry('j', new Boolean[]{false, true, true, true}),
			Map.entry('k', new Boolean[]{true, false, true}),
			Map.entry('l', new Boolean[]{false, true, false, false}),
			Map.entry('m', new Boolean[]{true, true}),
			Map.entry('n', new Boolean[]{true, false}),
			Map.entry('o', new Boolean[]{true, true, true}),
			Map.entry('p', new Boolean[]{false, true, true, false}),
			Map.entry('q', new Boolean[]{true, true, false, true}),
			Map.entry('r', new Boolean[]{false, true, false}),
			Map.entry('s', new Boolean[]{false, false, false}),
			Map.entry('t', new Boolean[]{true}),
			Map.entry('u', new Boolean[]{false, false, true}),
			Map.entry('v', new Boolean[]{false, false, false, true}),
			Map.entry('w', new Boolean[]{false, true, true}),
			Map.entry('x', new Boolean[]{true, false, false, true}),
			Map.entry('y', new Boolean[]{true, false, true, true}),
			Map.entry('z', new Boolean[]{true, true, false, false}),
			Map.entry('1', new Boolean[]{false, true, true, true, true}),
			Map.entry('2', new Boolean[]{false, false, true, true, true}),
			Map.entry('3', new Boolean[]{false, false, false, true, true}),
			Map.entry('4', new Boolean[]{false, false, false, false, true}),
			Map.entry('5', new Boolean[]{false, false, false, false, false}),
			Map.entry('6', new Boolean[]{true, false, false, false, false}),
			Map.entry('7', new Boolean[]{true, true, false, false, false}),
			Map.entry('8', new Boolean[]{true, true, true, false, false}),
			Map.entry('9', new Boolean[]{true, true, true, true, false}),
			Map.entry('0', new Boolean[]{true, true, true, true, true})
			);

	public static String translateChar(Character character) {
		if (!(Character.isAlphabetic(character) || Character.isDigit(character))) {
			return (character == ' ') ? "  " : character.toString();
		}
		String morseCharacter = new String();
		Boolean[] characterBools = MORSE_LETTERS.get(character);
		for (Boolean symbol : characterBools)
		{
			morseCharacter += symbol ? "_" : ".";
		}
		return morseCharacter;
	}
}

class MorseCodeTranslator {
	public static void main(String[] args) {
		Scanner inpScanner = new Scanner(System.in);
		try {
			while(true) {
				System.out.print("\nEnter text or enter '--quit' to quit: ");
				String text = inpScanner.nextLine().toLowerCase();
				if (text.equals("--quit")) break;
				System.out.println(text);
				for (Character c : text.toCharArray()) {
					System.out.printf("%s ", Translator.translateChar(c));
				}
			}
		} finally {
			inpScanner.close();
		}
	}
}
